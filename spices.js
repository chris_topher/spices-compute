'use strict'
class Spice {

  constructor(new_spice_count) {
    this.spice_count = (new_spice_count != null ? new_spice_count : 0);
  }
  toCredits()
  {
    return this.spice_count / 60;
  }
}

function computeSpices() {
  var spice_input = document.getElementById('spice_input');
  var spice = new Spice(spice_input.value);
  document.getElementById('spice_count_display').textContent = spice.toCredits().toString();
}
