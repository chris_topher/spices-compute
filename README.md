# README #

### What is this repository for ? ###
This is a tiny project for playing with JavaScript and DOM.
It converts Spice units to Epitech Credits.

### What are Spice units ? ###
At Epitech, A Spice unit represent 1 hour spent with School Associations.
Each time you acquire 60 Spices, you get 1 Epitech Credit.
At Epitech, You need credits to validate your year.
